#!/usr/bin/env python3

from github import Github, Auth, Repository

# Read Github acccess token from file
ghtoken_file = "ghtoken"
with open(ghtoken_file, "r") as file:
    ghtoken = file.read().replace("\n", "")

# Create Github client object
g = Github(auth=Auth.Token(ghtoken))

# Creating an instance of AuthenticatedUser and logging in
# is not necessary to read-access a public repo
#user = g.get_user()
#user.login

# Get list of releases for repo restic
#   For a list of attributes of repo or asset, see:
#   https://docs.github.com/en/rest/releases/releases?apiVersion=2022-11-28
repo = "restic/restic"
for rel in g.get_repo(repo).get_releases():
    print(f"{rel.title}, {rel.author.login}, {rel.published_at}, {rel.html_url}")
    # Print a list of all assets of the release
    for ass in rel.get_assets():
        print(f" - {ass.browser_download_url}")
    print("")

g.close()
